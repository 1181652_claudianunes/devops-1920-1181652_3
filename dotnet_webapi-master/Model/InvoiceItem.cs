﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FirstExercice.Model
{
    public class InvoiceItem
    {
        public int InvoiceItemId { get; set; }
        public int InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public string Code { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal Discount { get; set; } 
        public int IvaId { get; set; }
        [ForeignKey("IvaId")]

        public virtual Invoice Invoice { get; set; }
        public virtual IvaTax IvaTax { get; set; }

        public static implicit operator List<object>(InvoiceItem v)
        {
            throw new NotImplementedException();
        }
    }
}
