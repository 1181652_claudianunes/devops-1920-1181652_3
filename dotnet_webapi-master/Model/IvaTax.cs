﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FirstExercice.Model
{
    public class IvaTax
    {
        [Key]
        public int IvaId { get; set; }
        public double IvaValue { get; set; }
        public DateTime InitDate { get; set; }
        public DateTime FinalDate { get; set; }
        
        public virtual List<IvaTax> IvaTaxes { get; set; }
    }
}
