﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstExercice.Calculos
{
    public static class FazCal
    {
        public static decimal Calcula_linhas(int qtd, decimal prc, decimal desc)
        {
            decimal resultado = qtd * (prc - (prc * desc));

            if (resultado < 0)
            {
                throw new System.ArgumentException("Parameter cannot be lower then zero.", "original");
            }
            else
            {
                return resultado;
            }

        }



    }
}
