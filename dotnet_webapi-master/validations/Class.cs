﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstExercice.Model;

namespace FirstExercice.validations
{
    public class Class
    {

        public static bool ClassEmptyLine(Invoice invoice)
        {
            if ( invoice.Items == null || invoice.Items.Sum(x => x.Quantity) < 1)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public static bool ClassNullTotal(Invoice invoice)
        {
            if(invoice.Items == null || invoice.Items.Sum(x => x.Quantity * (x.Price - (x.Price * x.Discount)))<=0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
