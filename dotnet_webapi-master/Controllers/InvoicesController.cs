﻿using FirstExercice.Infrastructure;
using FirstExercice.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using FirstExercice.validations;
using FirstExercice.Calculos;

namespace FirstExercice.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {

        private readonly DbApiContext _context;
        public InvoicesController(DbApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = _context.Invoices.Select(
                   (i => new
                   {
                       i.InvoiceCode,
                       Date = i.Date.ToString("dd/MM/yyyy"),
                       CustomerName = i.Customer.FirstName + i.Customer.LastName,
                       CustomerContact = i.Customer.Contact,
                       TotalItens = i.Items.Sum(x => x.Quantity),
                       TotalValue = FazCal.Calcula_linhas(i.Items.Sum(x => x.Quantity), i.Items.Sum(x => x.Price), i.Items.Sum(x => x.Discount)),
                       IvaTax = i.Items.FirstOrDefault().IvaId


                   })) ;

            return Ok(response);
        }

        private object Calcula_linhas()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task<ActionResult<Invoice>> Add(Invoice invoice)
        { 
            if (invoice == null  || invoice.Customer == null || Class.ClassNullTotal(invoice) == true || Class.ClassEmptyLine(invoice) == true)
            { 
                return BadRequest();
            }
            try
            {
                invoice.Id = await _context.Invoices.MaxAsync(x => x.Id) + 1;
                await _context.Invoices.AddAsync(invoice);
                await _context.SaveChangesAsync();
                
                var response = new
                {
                    invoice.InvoiceCode
                };
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }

}

