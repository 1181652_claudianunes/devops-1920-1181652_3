﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstExercice.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
//using FirstExercice.Calculos;
using FirstExercice.Model;
using FirstExercice.validations;
using FirstExercice.Calculos;

namespace FirstExercice.Controllers.Tests
{
    [TestClass()]
    public class InvoicesControllerTests
    {
        [TestMethod()]
        public void AddTest()
        {
            int qtd = 2;
            decimal pric = 10 ;
            decimal desc = 0.5m;
            decimal expected = 10;
            Assert.AreEqual(expected, FazCal.Calcula_linhas(qtd, pric, desc));

        }
        [TestMethod()]
        public void TestNullTotal()
        {
            Invoice invoice = new Invoice();
            invoice.Id = 100;
            invoice.Date = DateTime.Now;
            invoice.InvoiceCode = "20191113";
            invoice.CustomerId = 2;
            invoice.Items = new List<InvoiceItem>();
            invoice.Items.Add(new InvoiceItem
            {
                InvoiceItemId = 2,
                InvoiceId = 100,
                Code = "7EIQ-72IU-2YNV-3L4Y",
                Price = 5,
                Quantity = 1,
                Discount = 1.25m,
                IvaId = 1,
            });

            Assert.IsTrue(Class.ClassNullTotal(invoice));
        }
        [TestMethod()]
        public void TestEmptyLine()
        {
            Invoice invoice = new Invoice();
            invoice.Id = 100;
            invoice.Date = DateTime.Now;
            invoice.InvoiceCode = "20191113";
            invoice.CustomerId = 2;
            invoice.Items = new List<InvoiceItem>();
            invoice.Items.Add(new InvoiceItem
            {
                InvoiceItemId = 2,
                InvoiceId = 100,
                Code = "7EIQ-72IU-2YNV-3L4Y",
                Price = 5,
                Quantity = 0,
                Discount = 1.25m,
                IvaId = 1,
            });

            Assert.IsTrue(Class.ClassEmptyLine(invoice));
        }
        [TestMethod()]
        public void TestNullTotalfalse()
        {
            Invoice invoice = new Invoice();
            invoice.Id = 100;
            invoice.Date = DateTime.Now;
            invoice.InvoiceCode = "20191113";
            invoice.CustomerId = 2;
            invoice.Items = new List<InvoiceItem>();
            invoice.Items.Add(new InvoiceItem
            {
                InvoiceItemId = 2,
                InvoiceId = 100,
                Code = "7EIQ-72IU-2YNV-3L4Y",
                Price = 5,
                Quantity = 1,
                Discount = 0.25m,
                IvaId = 1,
            });
            Assert.IsFalse(Class.ClassNullTotal(invoice));
        }
        [TestMethod()]
        public void TestEmptyLinefasle()
        {

            Invoice invoice = new Invoice();
            invoice.Id = 100;
            invoice.Date = DateTime.Now;
            invoice.InvoiceCode = "20191113";
            invoice.CustomerId = 2;
            invoice.Items = new List<InvoiceItem>();
            invoice.Items.Add(new InvoiceItem
            {
                InvoiceItemId = 20,
                InvoiceId = 100,
                Code = "7EIQ-72IU-2YNV-3L4Y",
                Price = 5,
                Quantity = 1,
                Discount = 0.25m,
                IvaId = 1,
            });
            Assert.IsFalse(Class.ClassEmptyLine(invoice));
        }
    }
}

